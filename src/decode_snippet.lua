local json = require 'dkjson';
local inspect = require 'inspect'; -- to print tables


--- Reads the content from the snippet json
-- @param path the path to the vscode snippet json
local function read_file(path)
    local file = io.open(path, "rb") -- r read mode and b binary mode
    if not file then return nil end
    local content = file:read "*a" -- *a or *all reads the whole file
    file:close()
    return content
end



--- Gets the content of prefix under which the snippet will be called from the json file
-- @param path the path to the vscode snippet json
-- TODO error handling https://www.tutorialspoint.com/lua/lua_error_handling.htm
local function getprefix(path)
  local fileContent = read_file(path);
  local obj, pos, err=json.decode(fileContent,1,nil)
  local prefixes = {}
  if err then
    print ("Error:", err)
  else
    for i, j  in pairs(obj) do
      table.insert(prefixes, j["prefix"])
    end
  end
  return prefixes;
end

--- Gets the content of snippet from the json file
-- @param path the path to the vscode snippet json
local function getcontent(path)
  local fileContent = read_file(path);
  local obj, pos, err=json.decode(fileContent,1,nil)
    
  local content = {}
  if err then
    print ("Error:", err)
  else
    for i, j  in pairs(obj) do
      -- table.insert(content, j["body"][1])
      -- print(inspect(j["body"][1]))
      tmpcontent = {}
      snippet = true
      k = 0 -- snippet line variable
      while(snippet)do
        k=k+1
        if j["body"][k] == nil then
          snippet = false
        end
        table.insert(tmpcontent, j["body"][k])
      end
      table.insert(content, tmpcontent)
      -- print(inspect(content));
      -- print("\n")
    end
  end
  return content;
end


return { getprefix = getprefix, getcontent = getcontent }
-- local tmp = getcontent("vscode-svelte-snippets/snippets/svelte.json")
-- print(inspect(tmp))
  -- print (obj["svelte-component-format"].prefix)
  -- print (obj["svelte-component-format"].description)

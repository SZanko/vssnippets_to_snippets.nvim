local decode_snippet = require 'src/decode_snippet';
local inspect = require 'inspect';

local function outfilename_without_extension(path)
	return path:match("(.+)%..+")
end

local function replace_noneuseable_chars(string)
	string = string.gsub(string,'-','_')
	string = string.gsub(string,':','_')
	string = string.gsub(string,'#','_')
	return string.gsub(string,'*','')
end

local function writeimports(output)
	local file = io.open(output,"w");
	io.output(file);
	local tmp = output
	local file_without_ext = outfilename_without_extension(tmp)
	io.write("local _"..file_without_ext.." = {}\n"..
		"local U = require'snippets.utils'\n\n")
end

local function generatesnippets(source)
	local prefix = decode_snippet.getprefix(source);
	local content = decode_snippet.getcontent(source);
	for i in pairs(prefix) do
		underscoredstring = replace_noneuseable_chars(prefix[i]);
		io.write("local _"..underscoredstring.." = [[\n")
		for j in pairs(content[i]) do
			io.write("\t"..content[i][j].."\n")
		end
		io.write("]]\n")
	end

end


--- Writes the Function which can be used to import in the snippet section
-- @param source path to vscode snippet json
-- @param output where the finished Snippet will be placed
local function generatesnippetsfunction(source, output)
	local prefix = decode_snippet.getprefix(source)

	io.write("function _"..outfilename_without_extension(output)..".snippets()\n"..
		"\treturn {\n");
	for i in pairs(prefix) do
		underscoredstring = replace_noneuseable_chars(prefix[i])
		io.write("\t\t"..underscoredstring.." = _"..underscoredstring..",\n");
	end
	io.write("\t}\n"..
		"end\n\n")
end

local function returnsnippets(output)
	io.write("return _"..outfilename_without_extension(output))
	io.close()
end

return { 
	writeimports=writeimports,
	generatesnippets=generatesnippets,
	generatesnippetsfunction=generatesnippetsfunction,
	returnsnippets=returnsnippets
}

-- !#/usr/bin/env lua

local argparse = require'argparse';
local write_snippetfile = require'src/write_snippet';

local parser = argparse() {
  name = "VSSnippets->Snippets.nvim",
  description = "A Converter which converts VSCode Snippets to Snippets.nvim Snippets",
  epilog = "For more info, take a look at the README.md"
}
parser:option "-s" "--source"
  :args(1)
parser:option "-o" "--output"
  :args(1)
local args = parser:parse()
write_snippetfile.writeimports(args.output)
write_snippetfile.generatesnippets(args.source)
write_snippetfile.generatesnippetsfunction(args.source, args.output)
write_snippetfile.returnsnippets(args.output)

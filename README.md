# VSCode Snippets to Snippets.nvim Converter

## Purpose

There are many snippets for VSCode available compared to [snippet.nvim](https://github.com/norcalli/snippets.nvim)
This Programm was created to convert these snippets to a format that snippet.nvim can read

## Dependencies

- [argparse](https://github.com/mpeterv/argparse#installation)
- [lua-dkjson](http://dkolf.de/src/dkjson-lua.fsl/home)

Which can be both installed in Archlinux with
```shell
sudo pacman -S lua-dkjson lua-argparse
```

Or with luarocks

```shell
sudo luarocks install dkjson argparse
```

## Installation

```shell
git clone https://gitlab.com/SZanko/vssnippets_to_snippets.nvim.git
cd vssnippets_to_snippets.nvim
```

## How to use it

### Generate Snippet.lua
```shell
lua src/commandline.lua -s path_to_snippet.json -o svelte.lua
mv svelte.lua ~/.config/nvim/lua/
```

### Neovim Config
```lua
require'snippet'.use_suggested_mappings()
local snippets = require'snippets'
local _svelte = require 'svelte';

snippets.snippets = {
    svelte = _svelte.snippets()
}
```



## Disclaimer

This whole programm was programmed in one afternoon &rarr; it is a little bit buggy,
and doesn't cover any edge cases I have only tested this program with the svelte vscode extension
